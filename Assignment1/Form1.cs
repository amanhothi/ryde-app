﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment1
{
    public partial class Form1 : Form
    {
       
        public Form1()
        {
            InitializeComponent();
        }
       
        private void button1_Click(object sender, EventArgs e)
        {
            string location1 = textBox1.Text;// getting values from textbox1
            string location2 = textBox2.Text;// getting values from textbox2
            double basefare = 2.50;
            double Chargeperkm = 0.81;
            double serviceCharge = 1.75;
            double distance = 0;
            double total = 0;
            string value = "";
           
            // if values textboxes are empty then show an error message
            if (location1 == "" || location2 == "")
            {
                MessageBox.Show("Both Locations are required to be filled");
            }
    
            else if (location1 == "275 YORKLAND BLVD" && location2 == "CN TOWER")
                {
                distance = 22.9;
                if (radioButton1.Checked == true)
                {
                    matchTime(); // method to check peak hours
                    poolRyde();// method to calculate pool ryde charge
                }  
                else if (radioButton2.Checked == true)// direct ride radio button
                {
                    matchTime();// method to check peak hours
                    Console.WriteLine(Chargeperkm);
                    directRyde();// method to calculate direct ryde
                    Console.WriteLine(Chargeperkm);
                    Console.WriteLine(basefare);
                }
                
                else if(radioButton1.Checked==false && radioButton2.Checked == false)
                {
                    MessageBox.Show("Select One Type of RYDE");
                }
                if (total < 5.50)// check for minimum fare
                {
                    total = 5.50;
                }
                Form2 Form2 = new Form2(location1, location2, basefare, Chargeperkm,
                           serviceCharge,distance,total,value);
                Form2.ShowDialog();

            }
            // second location and destinatiom
            else if (location1 == "FAIRVIEW MALL" && location2 == "TIM HORTONS")
                {
                distance = 1.2;
                if (radioButton1.Checked == true)
                {
                    matchTime();// calling method of peak hours
                    poolRyde();
                }
                else if (radioButton2.Checked == true)// for direct ride
                {
                     matchTime();
                     directRyde();
                   // Console.WriteLine(Chargeperkm);
                   // Console.WriteLine(basefare);
                }
                else if (radioButton1.Checked == false && radioButton2.Checked == false)
                {
                    MessageBox.Show("Select One Type of RYDE");
                }
                if (total < 5.50)// set minimum fare
                {
                    total = 5.50;
                }
                Form2 Form2 = new Form2(location1, location2, basefare, Chargeperkm,
                           serviceCharge,distance,total,value);
                Form2.ShowDialog();
                }
            else
                {
                  MessageBox.Show("Fill the correct locations in 'From ' and 'To' columns");
                }
            textBox1.Clear();// to clear textbox after execution of code
            textBox2.Clear();
           
            // creating methods 
             void matchTime()// creating method to match the time of peak hours
            {
                TimeSpan start1 = new TimeSpan(10, 0, 0); //10 o'clock
                TimeSpan end1 = new TimeSpan(12, 0, 0); //12 o'clock
                TimeSpan start2 = new TimeSpan(16, 0, 0); //4 o'clock or 4pm
                TimeSpan end2 = new TimeSpan(18, 0, 0); //6 o'clock or 6pm
                TimeSpan start3 = new TimeSpan(20, 0, 0); //8 o'clock or 8pm
                TimeSpan end3 = new TimeSpan(21, 0, 0); //9 o'clock or 9 pm
                TimeSpan now = DateTime.Now.TimeOfDay;

                Console.WriteLine(now);
                Console.WriteLine("Time Match Method Working");
                if ((now > start1) && (now < end1) || (now > start2) && (now < end2) ||
                    (now > start3) && (now < end3))
                {
                    Console.WriteLine("You are travelling in peak hours");
                    Chargeperkm = Chargeperkm + 0.16;// increase by 20%
                }
               
            }
            void poolRyde()// method for calculation of pool ryde
            {
                total = basefare + (Chargeperkm * distance) + serviceCharge;
                value = radioButton1.Text;
                Console.WriteLine(" Executing pool ryde method");
            }

            void directRyde()// method for calculation of direct ryde
            {
                basefare = basefare + 0.25;// increase by 10%
                Chargeperkm = Chargeperkm + 0.13;// increase by 15%
                total = basefare + (Chargeperkm * distance) + serviceCharge;
                value = radioButton2.Text;
                Console.WriteLine(" Executing direct ryde method");
            }

        }

    }
}



 