﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment1
{
    public partial class Form2 : Form
    {
        public Form2(string locationStart, string locationEnd, double baseFare,double
            distanceCharge,double serviceFees,double distance,double total,string value)
        {
            InitializeComponent();
            label1.Text = value + " Confirmed ";
            label2.Text = "From: " + locationStart;
            label3.Text = "To:   " + locationEnd;
            label4.Text = "Booking Fee :    $" + baseFare;
            label5.Text = "Distance Charge: $" + Math.Round((distanceCharge * distance), 2);
            label6.Text = "Service Fee :    $" + serviceFees;
            label7.Text = "Total :          $" + Math.Round(total, 2);
        }

        private void label1_Click(object sender, EventArgs e)
        {
            
        }

        private void Form2_Load(object sender, EventArgs e)
        { 
           
        }
    }
}
